# YaHORM

**Yet Another HBase ORM** is library that can derive implicit mappers for tables that are equivalent to some case
class (Product type).

## Usage

Should be as simple as deriving proper table mapper for product type.

```scala
import cats._
import shapeless._
import yahorm._
import org.apache.hbase.Table

case class Foo(
                      id: Long,
                      bar: String,
                      baz: Double,
                      qux: Int)

def doThingsOnTable(table: Table): Unit = {

  val tableMapper = TableMapperBuilder[Foo]
          .apply(ColumnId("t:bar") :: ColumnId("t:baz") :: ColumnId("t:qux") :: HNil)

  table.put(tableMapper.put(Foo(1, "text", 3.141592653589793, 42)))

  assert(
    tableMapper.parse(table.get(tableMapper.get(1))) ==
            Ior.Right(Foo(1, "text", 3.141592653589793, 42)))
}
```

### Defining custom serde

If for some reason default serializers are not sufficient for your use cases you can easily define your own by
implementing `Serializer[MyType]`, `CellDeseraializer[MyType]` or `BytesDeserializer[MyType]`.

```scala
import java.nio.ByteBuffer
import cats.implicits._


case class Foo(
  sequenceNr: Int,
  timestamp: Long,
  kind: Char)

object MySerde {

  implicit val fooSer: Serializer[Foo] =
    (value: Foo) =>
      ByteBuffer.allocate(13).putInt(value.sequenceNr).putLong(value.timestamp).putChar(value.kind).array()

  implicit val fooDe: CellDeserializer[Foo] =
    (value: Cell) => {
      if (cell.getValueLength() != 13) {
        NonEmptyChain.of(HError.ParseError(s"Can not deserializer Foo value, length of cell is ${cell.getValueLength()} != 13")).leftIor
      } else {
        val bb = ByteBuffer.wrap(cell.getValueArray(), cell.getValueOffset(), cell.getValueLength)
        Ior.Right(Foo(bb.getInt, bb.getLong, bb.getChar))
      }
    }
}

case class Bar(id: Long, foo: Foo, text: String, count: Int)

val barMapper = TableMapperBuilder[Bar].apply(
  TableName.valueOf("bar"), ColumnId("t:foo") :: ColumnId("t:text") :: ColumnId("t:count"))

val barTable: Table = connection.getTable(barMapper.tableName)

barMapper.parse(barTable.get(barMapper.mkGet(13L)))
```

## Package

*Not yet available*

## Development

### Future development

- [ ] Scan modifier in `TableMapper`
- [ ] Some refactoring is need to be done
  - [ ] ColumnId should be derivable from product types.
  - [ ] Improve names of methods and traits
- [x] Write some tests
- [ ] Make use of secondary key