package yahorm

import cats.data.{IorNec, NonEmptyChain}
import cats.implicits._
import yahorm.Column.IdentifiedColumn
import org.apache.hadoop.hbase.client.{Put, Result}
import shapeless.{Lazy, LowPriority}

trait ColumnBuilder[T] {
  def apply(wi: ColumnId): Column[T]
}

object ColumnBuilder {

  def apply[T](implicit cb: ColumnBuilder[T]): ColumnBuilder[T] = cb

  implicit def mkOptColBuilder[T](
    implicit
    ser: Lazy[Serializer[T]],
    de: Lazy[CellDeserializer[T]]
  ): ColumnBuilder[Option[T]] =
    (wi: ColumnId) => {
      new IdentifiedColumn[Option[T]](wi) {

        override def modifyPut(
          p: Put,
          maybeVal: Option[T]
        ): Put =
          maybeVal.foldLeft(p) { case (put, value) =>
            put.addColumn(wi.cf, wi.cq, ser.value(value))
          }

        override def parse(r: Result): IorNec[HError, Option[T]] =
          Option(r.getColumnLatestCell(wi.cf, wi.cq))
            .map(de.value(_).map(Some(_)))
            .getOrElse(None.rightIor)
      }

    }

  implicit def mkColBuilder[T](
    implicit
    lp: LowPriority,
    ser: Lazy[Serializer[T]],
    de: Lazy[CellDeserializer[T]]
  ): ColumnBuilder[T] =
    (wi: ColumnId) =>
      new IdentifiedColumn[T](wi) {

        override def modifyPut(
          p: Put,
          value: T
        ): Put = p.addColumn(wi.cf, wi.cq, ser.value(value))

        override def parse(r: Result): IorNec[HError, T] =
          Option(r.getColumnLatestCell(wi.cf, wi.cq))
            .map(de.value(_))
            .getOrElse(NonEmptyChain.one(HError.MissingCell(wi.cf, wi.cq)).leftIor)
      }
}
