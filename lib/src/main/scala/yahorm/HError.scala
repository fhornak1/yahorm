package yahorm

import org.apache.hadoop.hbase.util.Bytes

sealed trait HError extends Serializable {
  def message: String
}

object HError {

  final case object MissingRow extends HError {
    override def message: String =
      "HBase returned empty Result, which means such value may not be present in HBase."
  }

  final case class ParseError(message: String) extends HError

  final case class MissingCell(cf: ColumnFamily, cq: ColumnQualifier) extends HError {
    override def message: String =
      s"Missing mandatory column `${mkCol(cf, cq)}`"
  }

  private def mkCol(cf: ColumnFamily, cq: ColumnQualifier): String =
    s"${Bytes.toString(cf)}:${Bytes.toString(cq)}"
}
