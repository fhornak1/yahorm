package yahorm

import cats.data.IorNec
import org.apache.hadoop.hbase.client._
import shapeless._

object ColumnSetBuilder {

  type Aux[C <: HList, Cols0 <: HList] =
    ColumnSetBuilder[C] {
      type Cols = Cols0
    }

  def apply[C <: HList](implicit csb: ColumnSetBuilder[C]): Aux[C, csb.Cols] = csb

  implicit def singletonBuilder[H](
    implicit
    cb: ColumnBuilder[H]
  ): Aux[H :: HNil, ColumnId :: HNil] =
    new ColumnSetBuilder[H :: HNil] {
      override type Cols = ColumnId :: HNil

      override def apply(cols: Cols): ColumnSet[H :: HNil] = {
        val col = cb(cols.head)
        new ColumnSet[H :: HNil] {
          override def modifyPut(
            p: Put,
            value: H :: HNil
          ): Put = col.modifyPut(p, value.head)

          override def modifyGet(g: Get): Get = col.modifyGet(g)

          override def modifyDelete(d: Delete): Delete = col.modifyDelete(d)

          override def modifyScan(s: Scan): Scan = col.modifyScan(s)

          override def parse(r: Result): IorNec[HError, H :: HNil] = col.parse(r).map(_ :: HNil)
        }
      }
    }

  implicit def mkHListBuilder[H, T <: HList](
    implicit
    cb: Lazy[ColumnBuilder[H]],
    csb: ColumnSetBuilder[T]
  ): Aux[H :: T, ColumnId :: csb.Cols] =
    new ColumnSetBuilder[H :: T] {
      override type Cols = ColumnId :: csb.Cols

      override def apply(cols: Cols): ColumnSet[H :: T] = {
        val col    = cb.value(cols.head)
        val colSet = csb(cols.tail)
        new ColumnSet[H :: T] {
          override def modifyPut(
            p: Put,
            value: H :: T
          ): Put = colSet.modifyPut(col.modifyPut(p, value.head), value.tail)

          override def modifyGet(g: Get): Get = colSet.modifyGet(col.modifyGet(g))

          override def modifyDelete(d: Delete): Delete = colSet.modifyDelete(col.modifyDelete(d))

          override def modifyScan(s: Scan): Scan = colSet.modifyScan(col.modifyScan(s))

          override def parse(r: Result): IorNec[HError, H :: T] =
            combineIor(col.parse(r), colSet.parse(r))
        }
      }
    }

}

trait ColumnSetBuilder[C <: HList] {
  type Cols <: HList
  def apply(cols: Cols): ColumnSet[C]
}
