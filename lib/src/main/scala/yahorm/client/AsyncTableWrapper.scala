package yahorm.client

import cats.data.IorNec
import yahorm.{ TableMapper, HError}
import org.apache.hadoop.hbase.client.AsyncTable

import scala.concurrent.{ExecutionContext, Future}
import scala.compat.java8.FutureConverters._
import collection.JavaConverters._

object AsyncTableWrapper {
  type Aux[P, K] = AsyncTableWrapper[P] { type Key = K }
}

class AsyncTableWrapper[P](val table: AsyncTable[_])(implicit val ht: TableMapper[P], ec: ExecutionContext) {
  type Key = ht.PK

  def get(key: Key): Future[IorNec[HError, P]] =
    table.get(ht.mkGet(key)).toScala.map(ht.parse)

  def getAll(keys: Seq[Key]): Future[Seq[IorNec[HError, P]]] =
    table.getAll(keys.map(ht.mkGet).asJava).toScala.map(_.asScala.map(ht.parse))

  def put(value: P): Future[Unit] =
    table.put(ht.mkPut(value)).toScala.map(_ => ())

  def putAll(values: Seq[P]): Future[Unit] =
    table.putAll(values.map(ht.mkPut).asJava).toScala.map(_ => ())

  def delete(key: Key): Future[Unit] =
    table.delete(ht.mkDelete(key)).toScala.map(_ => ())

  def deleteAll(keys: Seq[Key]): Future[Unit] =
    table.deleteAll(keys.map(ht.mkDelete).asJava).toScala.map(_ => ())

  def scanAll: Future[Seq[IorNec[HError, P]]] =
    table.scanAll(ht.mkScan).toScala.map(_.asScala.map(ht.parse))

  def exists(key: Key): Future[Boolean] =
    table.exists(ht.mkGet(key)).toScala.map(Boolean.unbox)

  def existsAll(keys: Seq[Key]): Future[Seq[Boolean]] =
    table.existsAll(keys.map(ht.mkGet).asJava).toScala.map(_.asScala.map(Boolean.unbox))
}
