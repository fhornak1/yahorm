package yahorm.client

import cats.data.IorNec
import org.apache.hadoop.hbase.client.Table
import yahorm.{ HError, TableMapper }

import scala.collection.JavaConverters._

class TableWrapper[P](val table: Table)(implicit val mapper: TableMapper[P]) {
  type Key = mapper.PK

  def get(key: Key): IorNec[HError, P] = mapper.parse(table.get(mapper.mkGet(key)))

  def getAll(keys: Seq[Key]): Seq[IorNec[HError, P]] =
    table.get(keys.map(mapper.mkGet).asJava).map(mapper.parse)

  def put(value: P): Unit = table.put(mapper.mkPut(value))

  def putAll(values: Seq[P]): Unit = table.put(values.map(mapper.mkPut).asJava)

  def delete(key: Key): Unit = table.delete(mapper.mkDelete(key))

  def deleteAll(keys: Seq[Key]): Unit = table.delete(keys.map(mapper.mkDelete).asJava)

  def scanAll: Iterable[IorNec[HError, P]] = table.getScanner(mapper.mkScan).asScala.map(mapper.parse)

  def exists(key: Key): Boolean = table.exists(mapper.mkGet(key))

  def existsAll(keys: Seq[Key]): Seq[Boolean] = table.exists(keys.map(mapper.mkGet).asJava)
}
