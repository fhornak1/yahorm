package yahorm

import cats.data.IorNec
import cats.kernel.Semigroup
import org.apache.hadoop.hbase.TableName
import org.apache.hadoop.hbase.client.{AsyncConnection, Connection}
import shapeless.{::, HList, HNil}
import yahorm.client.{AsyncTableWrapper, TableWrapper}

import scala.concurrent.ExecutionContext

object Implicits {

  implicit final class AsyncConnectionExt(val connection: AsyncConnection) extends AnyVal {

    def asyncTable[P](
      implicit
      hTab: TableMapper[P],
      ec: ExecutionContext
    ): AsyncTableWrapper[P] = new AsyncTableWrapper(connection.getTable(hTab.tableName))
  }

  implicit final class ConnectionExt(val connection: Connection) extends AnyVal {

    def table[P](implicit hTab: TableMapper[P]): TableWrapper[P] =
      new TableWrapper[P](connection.getTable(hTab.tableName))
  }

  implicit final class TableBuilderOps1[P, Key](
    val b: TableMapperBuilder.Aux[P, Key, ColumnId :: HNil])
      extends AnyVal {

    def of(
      name: String,
      c1: ColumnId
    ): TableMapper.Aux[P, Key] = b.apply(TableName.valueOf(name), c1 :: HNil)
  }

  implicit final class TableBuilderOps2[P, Key](
    val b: TableMapperBuilder.Aux[P, Key, ColumnId :: ColumnId :: HNil])
      extends AnyVal {

    def of(
      name: String,
      c1: ColumnId,
      c2: ColumnId
    ): TableMapper.Aux[P, Key] = b.apply(TableName.valueOf(name), c1 :: c2 :: HNil)
  }

  implicit final class TableBuilderOps3[P, Key](
    val b: TableMapperBuilder.Aux[P, Key, ColumnId :: ColumnId :: ColumnId :: HNil])
      extends AnyVal {

    def of(
      name: String,
      c1: ColumnId,
      c2: ColumnId,
      c3: ColumnId
    ): TableMapper.Aux[P, Key] = b.apply(TableName.valueOf(name), c1 :: c2 :: c3 :: HNil)
  }

  implicit final class TableBuilderOps4[P, Key](
    val b: TableMapperBuilder.Aux[P, Key, ColumnId :: ColumnId :: ColumnId :: ColumnId :: HNil])
      extends AnyVal {

    def of(
      name: String,
      c1: ColumnId,
      c2: ColumnId,
      c3: ColumnId,
      c4: ColumnId
    ): TableMapper.Aux[P, Key] = b.apply(TableName.valueOf(name), c1 :: c2 :: c3 :: c4 :: HNil)
  }

  implicit final class TableBuilderOps5[P, Key](
    val b: TableMapperBuilder.Aux[
      P,
      Key,
      ColumnId :: ColumnId :: ColumnId :: ColumnId :: ColumnId :: HNil
    ])
      extends AnyVal {

    def of(
      name: String,
      c1: ColumnId,
      c2: ColumnId,
      c3: ColumnId,
      c4: ColumnId,
      c5: ColumnId
    ): TableMapper.Aux[P, Key] =
      b.apply(TableName.valueOf(name), c1 :: c2 :: c3 :: c4 :: c5 :: HNil)
  }

  implicit final class TableBuilderOps6[P, Key](
    val b: TableMapperBuilder.Aux[
      P,
      Key,
      ColumnId :: ColumnId :: ColumnId :: ColumnId :: ColumnId :: ColumnId :: HNil
    ])
      extends AnyVal {

    def of(
      name: String,
      c1: ColumnId,
      c2: ColumnId,
      c3: ColumnId,
      c4: ColumnId,
      c5: ColumnId,
      c6: ColumnId
    ): TableMapper.Aux[P, Key] =
      b.apply(TableName.valueOf(name), c1 :: c2 :: c3 :: c4 :: c5 :: c6 :: HNil)
  }

  implicit final class TableBuilderOps7[P, Key](
    val b: TableMapperBuilder.Aux[
      P,
      Key,
      ColumnId :: ColumnId :: ColumnId :: ColumnId :: ColumnId :: ColumnId :: ColumnId :: HNil
    ])
      extends AnyVal {

    def of(
      name: String,
      c1: ColumnId,
      c2: ColumnId,
      c3: ColumnId,
      c4: ColumnId,
      c5: ColumnId,
      c6: ColumnId,
      c7: ColumnId
    ): TableMapper.Aux[P, Key] =
      b.apply(TableName.valueOf(name), c1 :: c2 :: c3 :: c4 :: c5 :: c6 :: c7 :: HNil)
  }

  implicit final class TableBuilderOps8[P, Key](
    val b: TableMapperBuilder.Aux[
      P,
      Key,
      ColumnId :: ColumnId :: ColumnId :: ColumnId :: ColumnId :: ColumnId :: ColumnId ::
        ColumnId :: HNil
    ])
      extends AnyVal {

    def of(
      name: String,
      c1: ColumnId,
      c2: ColumnId,
      c3: ColumnId,
      c4: ColumnId,
      c5: ColumnId,
      c6: ColumnId,
      c7: ColumnId,
      c8: ColumnId
    ): TableMapper.Aux[P, Key] =
      b.apply(TableName.valueOf(name), c1 :: c2 :: c3 :: c4 :: c5 :: c6 :: c7 :: c8 :: HNil)
  }

  implicit final class TableBuilderOps9[P, Key](
    val b: TableMapperBuilder.Aux[
      P,
      Key,
      ColumnId :: ColumnId :: ColumnId :: ColumnId :: ColumnId :: ColumnId :: ColumnId ::
        ColumnId :: ColumnId :: HNil
    ])
      extends AnyVal {

    def of(
      name: String,
      c1: ColumnId,
      c2: ColumnId,
      c3: ColumnId,
      c4: ColumnId,
      c5: ColumnId,
      c6: ColumnId,
      c7: ColumnId,
      c8: ColumnId,
      c9: ColumnId
    ): TableMapper.Aux[P, Key] =
      b.apply(TableName.valueOf(name), c1 :: c2 :: c3 :: c4 :: c5 :: c6 :: c7 :: c8 :: c9 :: HNil)
  }

  implicit final class TableBuilderOps10[P, Key](
    val b: TableMapperBuilder.Aux[
      P,
      Key,
      ColumnId :: ColumnId :: ColumnId :: ColumnId :: ColumnId :: ColumnId :: ColumnId ::
        ColumnId :: ColumnId :: ColumnId :: HNil
    ])
      extends AnyVal {

    def of(
      name: String,
      c1: ColumnId,
      c2: ColumnId,
      c3: ColumnId,
      c4: ColumnId,
      c5: ColumnId,
      c6: ColumnId,
      c7: ColumnId,
      c8: ColumnId,
      c9: ColumnId,
      c10: ColumnId
    ): TableMapper.Aux[P, Key] =
      b.apply(
        TableName.valueOf(name),
        c1 :: c2 :: c3 :: c4 :: c5 :: c6 :: c7 :: c8 :: c9 :: c10 :: HNil)
  }
}
