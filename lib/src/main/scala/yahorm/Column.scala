package yahorm

import cats.data.IorNec
import org.apache.hadoop.hbase.client._

trait Column[T] {

  def modifyPut(
    p: Put,
    value: T
  ): Put

  def modifyGet(g: Get): Get

  def modifyDelete(d: Delete): Delete

  def modifyScan(s: Scan): Scan

  def parse(r: Result): IorNec[HError, T]
}

object Column {

  abstract class IdentifiedColumn[T](id: ColumnId) extends Column[T] {
    final override def modifyGet(g: Get): Get = g.addColumn(id.cf, id.cq)

    final override def modifyDelete(d: Delete): Delete = d.addColumn(id.cf, id.cq)

    final override def modifyScan(s: Scan): Scan = s.addColumn(id.cf, id.cq)
  }

}
