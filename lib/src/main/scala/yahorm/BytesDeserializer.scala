package yahorm

import cats.data.{Ior, IorNec, NonEmptyChain}
import cats.implicits._
import org.apache.hadoop.hbase.util.Bytes
import shapeless.LowPriority

trait BytesDeserializer[+T] {
  def apply(array: Array[Byte]): IorNec[HError, T]
}

object BytesDeserializer {
  def apply[T](implicit de: BytesDeserializer[T]): BytesDeserializer[T] = de

  implicit def stringDe(implicit lp: LowPriority): BytesDeserializer[String] =
    (array: Array[Byte]) =>
      de(HError.ParseError("Can not parse array as String"))(Bytes.toString(array))

  implicit def longDe(implicit lp: LowPriority): BytesDeserializer[Long] =
    (array: Array[Byte]) =>
      de(HError.ParseError("Can not parse array as Long"))(Bytes.toLong(array))

  implicit def intDe(implicit lp: LowPriority): BytesDeserializer[Int] =
    (array: Array[Byte]) => de(HError.ParseError("Can not parse array as Int"))(Bytes.toInt(array))

  implicit def shortDe(implicit lp: LowPriority): BytesDeserializer[Short] =
    (array: Array[Byte]) =>
      de(HError.ParseError("Can not parse array as Short"))(Bytes.toShort(array))

  implicit def booleanDe(implicit lp: LowPriority): BytesDeserializer[Boolean] = (array: Array[Byte]) =>
    de(HError.ParseError("Can not parse array as Boolean"))(Bytes.toBoolean(array))

  implicit def byteDe(implicit lp: LowPriority): BytesDeserializer[Byte] = (array: Array[Byte]) =>
    if (array.length != 1) {
      Ior.Left(NonEmptyChain.one(HError.ParseError("Can not parse array as Byte")))
    } else {
      Ior.Right(array(0))
    }

  implicit def floatDe(implicit lp: LowPriority): BytesDeserializer[Float] = (array: Array[Byte]) =>
    de(HError.ParseError("Can not parse array as Float"))(Bytes.toFloat(array))

  implicit def doubleDe(implicit lp: LowPriority): BytesDeserializer[Double] = (array: Array[Byte]) =>
    de(HError.ParseError("Can not parse array as Double"))(Bytes.toDouble(array))

  private def de[T](onError: => HError)(exec: => T): IorNec[HError, T] =
    Either.catchNonFatal(exec).leftMap(_ => onError).toIor.toIorNec
}
