package yahorm

import cats.data.{Ior, IorNec, NonEmptyChain}
import cats.implicits._
import org.apache.hadoop.hbase.{Cell, CellUtil}
import org.apache.hadoop.hbase.util.Bytes
import shapeless.LowPriority

import scala.annotation.{implicitAmbiguous, implicitNotFound}

@implicitNotFound("Can not find Cell deserializer for type [${T}]")
@implicitAmbiguous("Multiple Cell deserializers defined in the scope for type [${T}]")
trait CellDeserializer[+T] {
  def apply(cell: Cell): IorNec[HError, T]
}

object CellDeserializer {

  def apply[T](implicit de: CellDeserializer[T]): CellDeserializer[T] = de

  implicit def stringDe(implicit lp: LowPriority): CellDeserializer[String] = (cell: Cell) =>
    de(HError.ParseError("Failed to parse String from cell"))(Bytes.toString(cell.getValueArray, cell.getValueOffset, cell.getValueLength))

  implicit def longDe(implicit lp: LowPriority): CellDeserializer[Long] = (cell: Cell) =>
    de(HError.ParseError("Failed to parse Long from cell"))(Bytes.toLong(cell.getValueArray, cell.getValueOffset, cell.getValueLength))

  implicit def intDe(implicit lp: LowPriority): CellDeserializer[Int] = (cell: Cell) =>
    de(HError.ParseError("Failed to parse Int from cell"))(Bytes.toInt(cell.getValueArray, cell.getValueOffset, cell.getValueLength))

  implicit def shortDe(implicit lp: LowPriority): CellDeserializer[Short] = (cell: Cell) =>
    de(HError.ParseError("Failed to parse Short from cell"))(Bytes.toShort(cell.getValueArray, cell.getValueOffset, cell.getValueLength))

  implicit def doubleDe(implicit lp: LowPriority): CellDeserializer[Double] = (cell: Cell) =>
    de(HError.ParseError("Failed to parse Double from cell"))(Bytes.toDouble(cell.getValueArray, cell.getValueOffset))

  implicit def floatDe(implicit lp: LowPriority): CellDeserializer[Float] = (cell: Cell) =>
    de(HError.ParseError("Failed to parse Float from Cell"))(Bytes.toFloat(cell.getValueArray, cell.getValueOffset))

  implicit def booleanDe(implicit lp: LowPriority): CellDeserializer[Boolean] = (cell: Cell) =>
    de(HError.ParseError("Failed to parse Boolean from Cell"))(Bytes.toBoolean(CellUtil.cloneValue(cell)))

  implicit def byteDe(implicit lp: LowPriority): CellDeserializer[Byte] = (cell: Cell) =>
    if (cell.getValueLength != 1) {
      NonEmptyChain.one(HError.ParseError("Failed to parse Byte from Cell")).leftIor
    } else {
      Ior.Right(cell.getValueArray()(cell.getValueOffset))
    }

  implicit def arrayDe(implicit lowPriority: LowPriority): CellDeserializer[Array[Byte]] = (cell: Cell) =>
    Ior.Right(CellUtil.cloneValue(cell))

  implicit def fromBytesDe[T](implicit lp: LowPriority, bytesDe: BytesDeserializer[T]): CellDeserializer[T] = (cell: Cell) =>
    bytesDe(CellUtil.cloneValue(cell))

  private def de[T](onError: => HError)(value: => T): IorNec[HError, T] =
    Either.catchNonFatal(value)
      .leftMap(_ => onError)
      .toIor
      .toIorNec
}
