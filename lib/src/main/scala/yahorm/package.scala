import cats.data._
import cats.implicits._
import cats.kernel.Semigroup
import org.apache.hadoop.hbase.util.Bytes
import shapeless._
import shapeless.tag._

package object yahorm {
  sealed trait Primary

  type PrimaryKey[+T] = T @@ Primary

  object PrimaryKey {
    def apply[T](value: T): PrimaryKey[T] = value.asInstanceOf[PrimaryKey[T]]
  }

  sealed trait CF
  sealed trait CQ
  type ColumnFamily    = Array[Byte] @@ CF
  type ColumnQualifier = Array[Byte] @@ CQ

  object toCF extends Poly1 {

    implicit val caseColumnFamily: Case.Aux[ColumnFamily, ColumnFamily] = at[ColumnFamily] {
      identity
    }

    implicit val caseByteArray: Case.Aux[Array[Byte], ColumnFamily] = at[Array[Byte]] {
      _.asInstanceOf[ColumnFamily]
    }

    implicit val caseString: Case.Aux[String, ColumnFamily] = at[String] { s =>
      Bytes.toBytes(s).asInstanceOf[ColumnFamily]
    }
  }

  object toCQ extends Poly1 {

    implicit val caseColumnQualifier: Case.Aux[ColumnQualifier, ColumnQualifier] =
      at[ColumnQualifier] {
        identity
      }

    implicit val caseByteArray: Case.Aux[Array[Byte], ColumnQualifier] = at[Array[Byte]] {
      _.asInstanceOf[ColumnQualifier]
    }

    implicit val caseString: Case.Aux[String, ColumnQualifier] = at[String] { s =>
      Bytes.toBytes(s).asInstanceOf[ColumnQualifier]
    }
  }

  private[yahorm] def combineEither[L : Semigroup, H, T <: HList](
    h: => Either[L, H],
    t: => Either[L, T]
  ): Either[L, H :: T] =
    (h, t) match {
      case (Left(hErr), Left(tErr)) =>
        Left(hErr |+| tErr)
      case (Left(hErr), Right(_)) =>
        Left(hErr)
      case (Right(_), Left(tErr)) =>
        Left(tErr)
      case (Right(hVal), Right(tVal)) =>
        Right(hVal :: tVal)
    }

  private[yahorm] def combineIor[L : Semigroup, H, T <: HList](h: => Ior[L, H], t: => Ior[L, T]): Ior[L, H :: T] =
    (h, t) match {
      case (Ior.Left(hErr), Ior.Left(tErr)) => Ior.Left(hErr |+| tErr)
      case (Ior.Left(hErr), Ior.Both(tErr, _)) => Ior.Left(hErr |+| tErr)
      case (Ior.Left(hErr), Ior.Right(_)) => Ior.Left(hErr)
      case (Ior.Both(hErr, _), Ior.Left(tErr)) => Ior.Left(hErr |+| tErr)
      case (Ior.Both(hErr, hVal), Ior.Both(tErr, tVal)) => Ior.Both(hErr |+| tErr, hVal :: tVal)
      case (Ior.Both(hErr, hVal), Ior.Right(tVal)) => Ior.Both(hErr, hVal :: tVal)
      case (Ior.Right(_), Ior.Left(tErr)) => Ior.Left(tErr)
      case (Ior.Right(hVal), Ior.Both(tErr, tVal)) => Ior.Both(tErr, hVal :: tVal)
      case (Ior.Right(hVal), Ior.Right(tVal)) => Ior.Right(hVal :: tVal)
    }
}
