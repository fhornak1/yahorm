package yahorm

import shapeless._

trait ColumnSet[TT <: HList] extends Column[TT]
