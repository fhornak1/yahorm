package yahorm

object ByteSizeOf {

  def apply[T](implicit s: ByteSizeOf[T]): ByteSizeOf[T] = s

  implicit val byteSize: ByteSizeOf[Byte] = new ByteSizeOf[Byte] {
    override def bytes: Int = 1
  }

  implicit val boolSize: ByteSizeOf[Boolean] = new ByteSizeOf[Boolean] {
    override def bytes: Int = 1
  }

  implicit val shortSize: ByteSizeOf[Short] = new ByteSizeOf[Short] {
    override def bytes: Int = java.lang.Short.BYTES
  }

  implicit val intSize: ByteSizeOf[Int] = new ByteSizeOf[Int] {
    override def bytes: Int = java.lang.Integer.BYTES
  }

  implicit val longSize: ByteSizeOf[Long] = new ByteSizeOf[Long] {
    override def bytes: Int = java.lang.Long.BYTES
  }

  implicit val floatSize: ByteSizeOf[Float] = new ByteSizeOf[Float] {
    override def bytes: Int = java.lang.Float.BYTES
  }

  implicit val doubleSize: ByteSizeOf[Double] = new ByteSizeOf[Double] {
    override def bytes: Int = java.lang.Double.BYTES
  }
}

trait ByteSizeOf[T] {
  def bytes: Int
}
