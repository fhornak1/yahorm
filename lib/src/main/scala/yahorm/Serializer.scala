package yahorm

import java.nio.ByteBuffer
import org.apache.hadoop.hbase.util.{ByteBufferAllocator, Bytes}
import shapeless.LowPriority

trait Serializer[-T] {
  def apply(value: T): Array[Byte]
}

object Serializer {

  def apply[T](implicit ser: Serializer[T]): Serializer[T] = ser

  implicit def stringSer(implicit lp: LowPriority): Serializer[String] = Bytes.toBytes

  implicit def intSer(implicit lp: LowPriority): Serializer[Int] = Bytes.toBytes

  implicit def longSer(implicit lp: LowPriority): Serializer[Long] = Bytes.toBytes

  implicit def booleanSer(implicit lp: LowPriority): Serializer[Boolean] = Bytes.toBytes

  implicit def shortSer(implicit lp: LowPriority): Serializer[Short] = Bytes.toBytes

  implicit def byteSer(implicit lp: LowPriority): Serializer[Byte] =
    (value: Byte) => Array[Byte](value)

  implicit def bytesSer(implicit lp: LowPriority): Serializer[Array[Byte]] =
    identity

  implicit def doubleSer(implicit lp: LowPriority): Serializer[Double] = Bytes.toBytes

  implicit def floatSer(implicit lp: LowPriority): Serializer[Float] = Bytes.toBytes

  implicit def seqOf[T](implicit lowPriority: LowPriority, ser: Serializer[T], sizeOf: ByteSizeOf[T]): Serializer[Seq[T]] =
    (values: Seq[T]) => {
      val bb = ByteBuffer.allocate(4 + sizeOf.bytes * values.length)
      bb.putInt(values.length)
      values.foldLeft(bb) { case (buf, value) =>
        buf.put(ser(value))
      }.array()
    }
}
