package yahorm

import cats.data.IorNec
import org.apache.hadoop.hbase.TableName
import org.apache.hadoop.hbase.client._

trait TableMapper[T] {
  type PK

  def tableName: TableName

  def mkGet(key: PK): Get

  def mkScan: Scan

  def mkPut(value: T): Put

  def mkDelete(key: PK): Delete

  def parse(res: Result): IorNec[HError, T]
}

object TableMapper {

  type Aux[T, Key] =
    TableMapper[T] {
      type PK = Key
    }

  def apply[T](implicit tab: TableMapper[T]): Aux[T, tab.PK] = tab
}
