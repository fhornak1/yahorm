package yahorm

import cats.data.{IorNec, NonEmptyChain}
import cats.implicits.catsSyntaxIorId
import org.apache.hadoop.hbase.TableName
import org.apache.hadoop.hbase.client.{Delete, Get, Put, Result, Scan}
import shapeless._

object TableMapperBuilder {

  type Aux[P, PK0, Cols0 <: HList] =
    TableMapperBuilder[P] {
      type Row      = PK0
      type ColNames = Cols0
    }

  def apply[P](implicit tb: TableMapperBuilder[P]): Aux[P, tb.Row, tb.ColNames] = tb

  implicit def mkHListTableBuilder[R, Cols <: HList](
    implicit
    rowSer: Lazy[Serializer[R]],
    rowDe: Lazy[BytesDeserializer[R]],
    csb: ColumnSetBuilder[Cols]
  ): Aux[R :: Cols, R, csb.Cols] =
    new TableMapperBuilder[R :: Cols] {
      override type Row      = R
      override type ColNames = csb.Cols

      override def apply(name: TableName, cols: ColNames): TableMapper.Aux[R :: Cols, Row] = {
        val colSet = csb(cols)
        new TableMapper[R :: Cols] {
          override type PK = Row

          override def tableName: TableName = name

          override def mkGet(key: PK): Get = colSet.modifyGet(new Get(rowSer.value(key)))

          override def mkScan: Scan = colSet.modifyScan(new Scan())

          override def mkPut(value: R :: Cols): Put =
            colSet.modifyPut(new Put(rowSer.value(value.head)), value.tail)

          override def mkDelete(key: PK): Delete = colSet.modifyDelete(new Delete(rowSer.value(key)))

          override def parse(res: Result): IorNec[HError, R :: Cols] = {
            if (res.isEmpty) {
              NonEmptyChain.one(HError.MissingRow).leftIor
            } else {
              combineIor(rowDe.value(res.getRow), colSet.parse(res))
            }
          }
        }
      }
    }

  implicit def mkProdTableBuilder[P <: Product, PRepr <: HList](
    implicit
    gen: Generic.Aux[P, PRepr],
    ltb: TableMapperBuilder[PRepr]
  ): TableMapperBuilder.Aux[P, ltb.Row, ltb.ColNames] =
    new TableMapperBuilder[P] {
      override type Row      = ltb.Row
      override type ColNames = ltb.ColNames

      override def apply(name: TableName, cols: ColNames): TableMapper.Aux[P, Row] = {
        val tab = ltb.apply(name, cols)
        new TableMapper[P] {
          override type PK = Row

          override def tableName: TableName = tab.tableName

          override def mkGet(key: PK): Get = tab.mkGet(key)

          override def mkScan: Scan = tab.mkScan

          override def mkPut(value: P): Put = tab.mkPut(gen.to(value))

          override def mkDelete(key: PK): Delete = tab.mkDelete(key)

          override def parse(res: Result): IorNec[HError, P] = tab.parse(res).map(gen.from)
        }
      }
    }

}

trait TableMapperBuilder[P] {
  type Row
  type ColNames <: HList

  def apply(name: TableName, cols: ColNames): TableMapper.Aux[P, Row]
}
