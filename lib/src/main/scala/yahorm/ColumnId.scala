package yahorm

import shapeless.Poly1

trait ColumnId {
  def cf: ColumnFamily

  def cq: ColumnQualifier
}

object ColumnId extends Poly1 {

  implicit val caseString: Case.Aux[String, ColumnId] = at[String](cidFromString)

  private def cidFromString(value: String): ColumnId = {
    val idx = value.indexOf(':'.toInt)
    if (idx < 0) {
      throw new IllegalArgumentException("String must contain at least one `:` separator")
    }
    val cf = toCF(value.substring(0, idx))
    val cq = toCQ(value.substring(idx + 1, value.length))
    Impl(cf, cq)
  }

  implicit val casePairOfStrings: Case.Aux[(String, String), ColumnId] = at[(String, String)] { v =>
    val cf = toCF(v._1)
    val cq = toCQ(v._2)
    Impl(cf, cq)
  }

  implicit val casePairOfArrays: Case.Aux[(Array[Byte], Array[Byte]), ColumnId] =
    at[(Array[Byte], Array[Byte])] { v =>
      val cf = toCF(v._1)
      val cq = toCQ(v._2)
      Impl(cf, cq)
    }

  case class Impl(
    cf: ColumnFamily,
    cq: ColumnQualifier)
      extends ColumnId

}
