package yahorm

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.{HBaseTestingUtility, TableName}
import org.apache.hadoop.hbase.client.Put

import scala.collection.JavaConverters._

trait HBaseIt {
  protected lazy val hbase: HBaseTestingUtility = new HBaseTestingUtility()

  def withHBasePrepared[T, U](lazyIter: => Seq[T], cf: String)(tm: Configuration => U)(implicit mapper: TableMapper[T]): Unit =
    withHBasePrepared(lazyIter, mapper.mkPut, mapper.tableName, cf)(tm)

  def withHBasePrepared[T, U](lazyIter: => Seq[T], mkPut: T => Put, tableName: TableName, cf: String)(tm: Configuration => U): Unit = {
    withEmptyTable(tableName, cf) { cfg =>
      val connection = hbase.getConnection
      val table = connection.getTable(tableName)
      try {
        table.put(lazyIter.map(mkPut).asJava)
        hbase.flush()
      } finally table.close()
      tm(cfg)
    }
  }

  def withEmptyTable[U](tableName: TableName, cf: String)(tm: Configuration => U): Unit = {
    hbase.createTable(tableName, cf)
    try {
      tm(hbase.getConfiguration)
    } finally {
      hbase.deleteTableIfAny(tableName)
    }
  }

  def withHBase[U](tm: Configuration => U): Unit = tm(hbase.getConfiguration)

}
