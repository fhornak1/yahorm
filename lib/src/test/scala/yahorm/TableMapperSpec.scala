package yahorm

import cats.data.{Ior, NonEmptyChain}
import cats.implicits._
import org.apache.hadoop.hbase.TableName
import org.apache.hadoop.hbase.client.Put
import org.apache.hadoop.hbase.util.Bytes
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.scalatestplus.junit.JUnitRunner
import shapeless._

import scala.collection.JavaConverters._
import scala.util.{Success, Try}

@RunWith(classOf[JUnitRunner])
class TableMapperSpec extends AnyWordSpec with Matchers with HBaseIt with BeforeAndAfterAll {



  "TableMapper" should {
    val DefaultCF: Array[Byte] = Bytes.toBytes("t")

    "store data into hbase" in withEmptyTable(TableName.valueOf("test1"), "t") { conf =>

      case class Foo(id: Long, text: String, value: Double)

      val mapper = TableMapperBuilder[Foo].apply(TableName.valueOf("test1"), ColumnId("t:txt") :: ColumnId("t:val") :: HNil)

      val connection = hbase.getConnection

      val table = connection.getTable(TableName.valueOf("test1"))

      val toPut = List(
        Foo(1, "first", 0.0),
        Foo(2, "second", 1.0),
        Foo(3, "third", 3.14158265)
      )

      Try(table.put(toPut.map(mapper.mkPut).asJava)) should equal(Success(()))

      Try(table.get(mapper.mkGet(2))).map(mapper.parse) should equal(Success(Ior.Right(Foo(2, "second", 1.0))))
    }

    case class Bar(id: String, otherId: Long, count: Int)

    val data2 = Seq(
      Bar("first", 123L, 12),
      Bar("second", 223L, 0),
      Bar("third", 789L, 12)
    )

    def serializeBar(b: Bar): Put =
      new Put(Bytes.toBytes(b.id)).addColumn(DefaultCF, Bytes.toBytes("oid"), Bytes.toBytes(b.otherId))
        .addColumn(DefaultCF, Bytes.toBytes("cnt"), Bytes.toBytes(b.count))

    "load data from HBase" in  withHBasePrepared(data2, serializeBar,TableName.valueOf("test2"), "t") { conf =>
      val mapper = TableMapperBuilder[Bar].apply(TableName.valueOf("test2"), ColumnId("t:oid") :: ColumnId("t:cnt") :: HNil)

      val connection = hbase.getConnection

      val table = connection.getTable(TableName.valueOf("test2"))

      Try(table.get(mapper.mkGet("third"))).map(mapper.parse) should equal(Success(Ior.Right(Bar("third", 789L, 12))))
    }

    "delete data from HBase" in withHBasePrepared(data2, serializeBar, TableName.valueOf("test3"), "t") { conf =>
      val mapper = TableMapperBuilder[Bar].apply(TableName.valueOf("test3"), ColumnId("t:oid") :: ColumnId("t:cnt") :: HNil)

      val connection = hbase.getConnection

      val table = connection.getTable(TableName.valueOf("test3"))

      Try(table.delete(mapper.mkDelete("second"))) should equal(Success(()))

      Try(table.get(mapper.mkGet("second"))).map(mapper.parse) should equal(Success(NonEmptyChain.one(HError.MissingRow).leftIor))
    }
  }

  override protected def beforeAll(): Unit = {
    hbase.startMiniCluster()
  }

  override protected def afterAll(): Unit = {
    hbase.shutdownMiniCluster()
  }
}
