# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- All necessary files to easily build this project
- Basic implementation of yahorm. This implementation can be used to generate
  proper put, get, delete, scan requests and parse results.
- [README.md](README.md) with basic description and example
- This [CHANGELOG.md](CHANGELOG.md)